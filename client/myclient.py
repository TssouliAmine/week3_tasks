import requests

# base url
url = "http://127.0.0.1:3000/"

# header for text/html type
headers = {"Content-Type": "text/html; charset=utf-8"}

def menu():
  
    while True:

        print('''\nChoose one option from the list below\n
        1 - Endpoint '/'           (Task 1)  
        2 - Endpoint '/data'       (Task 2)
        3 - Endpoint '/name-query' (Task 3)
        4 - Endpoint '/rgb-to-hex' (Task 4)
        5 - Endpoint '/add'        (Task 5)
        6 - Endpoint '/hex-to-rgb' (Task 6)
        7 - All Endpoints at once  (All Tasks)
        0 - Exit the program\n''')
        print("-------------------------------------------------------\n")
        try:
            option = int(input('Enter an option: '))
        except:
            print('\nGoodbye, See you soon')
            exit()
            

        match option:
            case 1:
                home()
            case 2:
                data()
            case 3:
                name_query()
            case 4:
                rgb_to_hex()
            case 5:
                add()
            case 6:
                hex_to_rgb()
            case 7:
                all_tasks()    
            case 0:
                print('\nGoodbye, See you soon')
                exit()                    
            case _:
                print('\nPlease choose a number from 0 to 7')
                menu()     

# Endpoint /
def home():
    # Sending request to the server
    response = requests.get(url,headers=headers)
    # printing the response recieved from the server
    formatted_response(response.text)

# Endpoint /data
def data():
    # Sending request to the server
    response = requests.get(url+'data',headers=headers)
    # printing the response recieved from the server
    formatted_response(response.text)

# Endpoint /name-query
def name_query():
    query= {'fname':'Marshall','lname':'Mathers'}
    # Sending request to the server
    response = requests.get(url+'name-query',headers=headers,params=query)
    # printing the response recieved from the server
    formatted_response(response.text)

# Endpoint /rgb-to-hex
def rgb_to_hex():
    query= {'red':0,'green':0,'blue':255}
    # Sending request to the server
    response = requests.get(url+'rgb-to-hex',headers=headers,params=query)
    # printing the response recieved from the server
    formatted_response(response.text)

# Endpoint /add
def add():
    payload = {'num1':6.8,'num2':12.9,'client_name':'Varg Vikernes'} 
    # Sending request to the server
    response = requests.post(url+'add',headers={"Content-Type": "application/json"},json=payload)  
    # printing the response recieved from the server
    formatted_response(response.text)

# Endpoint /hex-to-rgb
def hex_to_rgb():
    query= {'hex':'#0A14FF'}
    # Sending request to the server
    response = requests.get(url+'hex-to-rgb',headers=headers,params=query)
    # printing the response recieved from the server
    formatted_response(response.json())

# request all entries at once    
def all_tasks():
    home()
    data()
    name_query()
    rgb_to_hex()
    add()
    hex_to_rgb()

# formatting the response in a visible way
def formatted_response(response):
    print('---------------------------------------------------\n\n')
    print(response)    
    print('\n\n---------------------------------------------------')

if __name__ == "__main__":
    menu()
       
        










