import {rgbToHex, hexToRgb} from "./converter.js";
import express from "express";
const app = express();
const port = 3000;
const text_html = 'text/html; charset=utf-8'
const app_json ='application/json'

// Allowing the app to parse Json request
app.use(express.json());

app.get("/", (req, res) => {
    // Specifying the content-type of the response
    res.setHeader('Content-Type', text_html);
    // Sending the response as text
    res.send('Got your message');

});

app.get("/data", (req, res) => {
    // Specifying the content-type of the response
    res.setHeader('Content-Type', text_html);
    // Sending the response as text
    res.send('Here is your data: 123');

});

app.get("/name-query", (req, res) => {
    // Deconstructing the Request Query
    const { fname, lname } = req.query;
    // Specifying the content-type of the response
    res.setHeader('Content-Type', text_html);
    // Sending the response as text
    res.send(`Your name is ${fname} ${lname}`);

});

app.get("/rgb-to-hex", (req, res) => {
    // Deconstructing the Request Query
    const {red, green, blue } = req.query;
    const hex = rgbToHex(red,green,blue);
    // Specifying the content-type of the response
    res.setHeader('Content-Type', text_html);
    // Sending the response as text
    res.send(`HEX value: ${hex}`);

});

app.post("/add", (req, res) => {
    // Deconstructing the Request Query
    const {num1, num2, client_name } = req.body;
    // Specifying the content-type of the response
    res.setHeader('Content-Type', text_html);
    // Sending the response as text
    res.send(`Client: ${client_name} sum is ${num1 + num2} `)

});

app.get("/hex-to-rgb", (req, res) => {
    // Deconstructing the Request Query
    const {hex} = req.query;
    const payload= hexToRgb(hex.substring(1));
    // Specifying the content-type of the response
    res.setHeader('Content-Type', app_json);
    // Sending the response as Json
    res.json(payload)

});

app.listen(port, () => {
    console.log(`The server is running at port ${port}`);
});