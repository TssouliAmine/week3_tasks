# INTRODUCTION

Communication between two programs over the network using HTTP, base url is http://localhost:3000, there is 6 endpoints, as you can see below:

* /
* /data
* /name-query
* /rgb-to-hex
* /add
* /hex-to-rgb

# SPECIFICATIONS

- Client-side program with Python
- Server-side program with NodeJS

# SCREENSHOT

![Killing time ](/screenshot_program.png "Nothing fancy")

